# MyMoney

MyMoney merupakan suatu bot line dimana pengguna dapat menyimpan data keuangannya di sini.

Fitur:
1. **Catat Pendapatan** 
    - Muhammad Luthfi Fahlevi (1906293215)
2. **Catat Pengeluaran** 
    - Kevin Dharmawan (1906398515)
3. **Notifikasi** 
    - Dimas Saputra (1506688821)
4. **Laporan** 
    - Maheswara Ananta Argono (1906398471)
5. **Kalkulator Investasi** 
    - Linus Abhyasa Wicaksana (1906398761)
    
