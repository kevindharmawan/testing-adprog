package com.c13adprog.mymoney.repository;

import static org.mockito.Mockito.when;

import com.c13adprog.mymoney.database.ExpenseDao;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class ExpenseDbTest {

    @Mock
    private ExpenseDao expenseDao;

    @InjectMocks
    private ExpenseDb expenseDb;

    @Test
    void testExpenseGetByUserIdTest() {
        when(expenseDao.getByUserId("userid")).thenReturn(new ArrayList<>());
        Assert.assertTrue(expenseDb.getByUserId("userid") instanceof ArrayList);
    }
}
