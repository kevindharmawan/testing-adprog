package com.c13adprog.mymoney.model;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ExpenseTest {

    private Expense expense;

    @BeforeEach
    public void setup() throws Exception {
        expense = new Expense(
                "userid",
                "title",
                "category",
                "timestamp",
                "10000"
        );
    }

    @Test
    public void testExpenseGetUserId() {
        Assert.assertEquals("userid", expense.getUserId());
    }

    @Test
    public void testExpenseSetUserId() {
        Assert.assertEquals("userid", expense.getUserId());
        expense.setUserId("newuserid");
        Assert.assertEquals("newuserid", expense.getUserId());
    }

    @Test
    public void testExpenseGetTitle() {
        Assert.assertEquals("title", expense.getTitle());
    }

    @Test
    public void testExpenseSetTitle() {
        Assert.assertEquals("title", expense.getTitle());
        expense.setTitle("newtitle");
        Assert.assertEquals("newtitle", expense.getTitle());
    }

    @Test
    public void testExpenseGetCategory() {
        Assert.assertEquals("category", expense.getCategory());
    }

    @Test
    public void testExpenseSetCategory() {
        Assert.assertEquals("category", expense.getCategory());
        expense.setCategory("newcategory");
        Assert.assertEquals("newcategory", expense.getCategory());
    }

    @Test
    public void testExpenseGetTimestamp() {
        Assert.assertEquals("timestamp", expense.getTimestamp());
    }

    @Test
    public void testExpenseSetTimestamp() {
        Assert.assertEquals("timestamp", expense.getTimestamp());
        expense.setTimestamp("newtimestamp");
        Assert.assertEquals("newtimestamp", expense.getTimestamp());
    }

    @Test
    public void testExpenseGetNominal() {
        Assert.assertEquals("10000", expense.getNominal());
    }

    @Test
    public void testExpenseSetNominal() {
        Assert.assertEquals("10000", expense.getNominal());
        expense.setNominal("20000");
        Assert.assertEquals("20000", expense.getNominal());
    }
}
