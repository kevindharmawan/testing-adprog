package com.c13adprog.mymoney.database;

import com.c13adprog.mymoney.model.Record;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class RecordDaoImp implements RecordDao {
    private JdbcTemplate jdbcTemplate;
    private static final String RECORD_TABLE = "tbl_record";
    private static final String SQL_SELECT_ALL = "SELECT * FROM " + RECORD_TABLE;
    private static final ResultSetExtractor<List<Record>>
            RECORD_EXTRACTOR = RecordDaoImp::extractData;

    public RecordDaoImp(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * extract data for record table
     **/
    public static List<Record> extractData(ResultSet resultSet) throws SQLException {
        List<Record> extractList = new ArrayList<Record>();
        while (resultSet.next()) {
            Record sp = new Record(
                    resultSet.getString("user_id"),
                    resultSet.getString("name"),
                    resultSet.getString("category"),
                    resultSet.getString("timestamp"),
                    resultSet.getString("nominal"));
            extractList.add(sp);
        }
        return extractList;
    }

    /**
     * function for record database
     **/
    @Override
    public List<Record> get() {
        return jdbcTemplate.query(SQL_SELECT_ALL, RECORD_EXTRACTOR);
    }

    @Override
    public List<Record> getByUserId(String userId) {
        String SQL_GET_BY_USER_ID = SQL_SELECT_ALL + " WHERE LOWER(user_id) LIKE LOWER(?);";
        return jdbcTemplate.query(SQL_GET_BY_USER_ID,
                new Object[]{"%" + userId + "%"},
                RECORD_EXTRACTOR);
    }

    @Override
    public int saveRecord(String userId, String name, String category,
                          String timestamp, String nominal) {
        String REGISTER = "INSERT INTO " + RECORD_TABLE
                + " (user_id, name, category, timestamp, nominal) VALUES (?, ?, ?, ?, ?);";
        return jdbcTemplate.update(REGISTER, userId, name, category, timestamp, nominal);
    }
}
