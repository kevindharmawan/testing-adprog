package com.c13adprog.mymoney.database;

import com.c13adprog.mymoney.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImp implements UserDao {
    private JdbcTemplate jdbcTemplate;
    private static final String USER_TABLE = "tbl_user";
    private static final String SQL_SELECT_ALL = "SELECT * FROM " + USER_TABLE;
    private static final ResultSetExtractor<List<User>>
            USER_EXTRACTOR = UserDaoImp::extractData;

    public UserDaoImp(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * extract data user table
     **/
    public static List<User> extractData(ResultSet resultSet) throws SQLException {
        List<User> extractList = new ArrayList<>();
        while (resultSet.next()) {
            User p = new User(
                    resultSet.getString("userId"),
                    resultSet.getString("name"),
                    resultSet.getString("remind"));
            extractList.add(p);
        }
        return extractList;
    }

    /**
     * function for user database
     **/
    @Override
    public int registerUser(String userId, String name) {
        String REGISTER = "INSERT INTO " + USER_TABLE
                + " (user_id, name, remind) VALUES (?, ?, ?);";
        return jdbcTemplate.update(REGISTER, userId, name, "false");
    }

    @Override
    public List<User> get() {
        return jdbcTemplate.query(SQL_SELECT_ALL, USER_EXTRACTOR);
    }

    @Override
    public List<User> getByUserId(String userId) {
        String SQL_GET_BY_USER_ID = SQL_SELECT_ALL + " WHERE LOWER(user_id) LIKE LOWER(?);";
        return jdbcTemplate.query(SQL_GET_BY_USER_ID, new Object[]{"%" + userId + "%"},
                USER_EXTRACTOR);
    }

    @Override
    public List<User> getStatusNotifikasiByUserId(String userId) {
        String SQL_GET_STATUS_NOTIFIKASI = SQL_SELECT_ALL + " WHERE LOWER(user_id) LIKE LOWER(?);";
        return jdbcTemplate.query(SQL_GET_STATUS_NOTIFIKASI, new Object[]{"%" + userId + "%"},
                USER_EXTRACTOR);
    }

    @Override
    public int setStatusNotifikasiByUserId(String status, String userId) {
        String SQL_UPDATE_STATUS_NOTIFIKASI = "UPDATE " + USER_TABLE
                + " SET remind = ? WHERE LOWER(user_id) LIKE LOWER(?);";
        return jdbcTemplate.update(SQL_UPDATE_STATUS_NOTIFIKASI, status, "%" + userId + "%");
    }

    @Override
    public List<User> getAllUserNotifikasiAktif() {
        String SQL_GET_ALL_NOTIFIKASI_TRUE = SQL_SELECT_ALL + " WHERE remind='true';";
        return jdbcTemplate.query(SQL_GET_ALL_NOTIFIKASI_TRUE, USER_EXTRACTOR);
    }
}
