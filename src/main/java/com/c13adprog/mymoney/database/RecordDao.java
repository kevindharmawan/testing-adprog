package com.c13adprog.mymoney.database;

import com.c13adprog.mymoney.model.Record;

import java.util.List;

public interface RecordDao {
    List<Record> get();

    List<Record> getByUserId(String userId);

    int saveRecord(String userId, String name, String category,
                   String timestamp, String nominal);
}