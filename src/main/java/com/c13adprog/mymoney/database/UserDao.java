package com.c13adprog.mymoney.database;

import com.c13adprog.mymoney.model.User;

import java.util.List;

public interface UserDao {
    List<User> get();

    List<User> getByUserId(String userId);

    List<User> getStatusNotifikasiByUserId(String userId);

    int registerUser(String userId, String displayName);

    int setStatusNotifikasiByUserId(String status, String userId);

    List<User> getAllUserNotifikasiAktif();
}
