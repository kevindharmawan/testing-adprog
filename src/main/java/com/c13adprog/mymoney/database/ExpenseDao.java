package com.c13adprog.mymoney.database;

import com.c13adprog.mymoney.model.Expense;

import java.util.List;

public interface ExpenseDao {
    List<Expense> get();

    List<Expense> getByUserId(String userId);

    int saveExpense(String userId, String title, String category, String timestamp, String nominal);
}
