package com.c13adprog.mymoney.database;

import com.linecorp.bot.client.LineClientConstants;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.client.LineSignatureValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.sql.DataSource;

@Configuration
@EnableScheduling
@PropertySource("classpath:application.properties")
public class DaoManager {
    @Autowired
    private Environment env;

    @Bean(name = "com.linecorp.channel_secret")
    public String getChannelSecret() {
        return env.getProperty("com.linecorp.channel_secret");
    }

    @Bean(name = "com.linecorp.channel_access_token")
    public String getChannelAccessToken() {
        return env.getProperty("com.linecorp.channel_access_token");
    }

    /**
     * Line Messaging Client
     **/
    @Bean(name = "lineMessagingClient")
    public LineMessagingClient getMessagingClient() {
        return LineMessagingClient
                .builder(getChannelAccessToken())
                .apiEndPoint(LineClientConstants.DEFAULT_API_END_POINT)
                .connectTimeout(LineClientConstants.DEFAULT_CONNECT_TIMEOUT_MILLIS)
                .readTimeout(LineClientConstants.DEFAULT_READ_TIMEOUT_MILLIS)
                .writeTimeout(LineClientConstants.DEFAULT_WRITE_TIMEOUT_MILLIS)
                .build();
    }

    @Bean(name = "lineSignatureValidator")
    public LineSignatureValidator getSignatureValidator() {
        return new LineSignatureValidator(getChannelSecret().getBytes());
    }

    @Bean
    DataSource getDataSource() {
        String dbUrl = System.getenv("JDBC_DATABASE_URL");
        String username = System.getenv("JDBC_DATABASE_USERNAME");
        String password = System.getenv("JDBC_DATABASE_PASSWORD");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        return dataSource;
    }

    @Bean
    public UserDao getUserDao() {
        return new UserDaoImp(getDataSource());
    }

    @Bean
    public RecordDao getRecordDao() {
        return new RecordDaoImp(getDataSource());
    }

    @Bean
    public ExpenseDao getExpenseDao() {
        return new ExpenseDaoImpl(getDataSource());
    }
}