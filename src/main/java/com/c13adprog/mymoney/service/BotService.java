package com.c13adprog.mymoney.service;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;

import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BotService {

    public Source source;

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private BotFlex botFlex;

//    @Autowired
//    private UserDb userService;
//
//    @Autowired
//    private ExpenseDb spendingService;

//    private HashMap<String, Handler> currentHandler = new HashMap<>();

    public void greetingMessage(String replyToken) {
//        registerUser(source);
        replyFlexMenu(replyToken);
    }

//    private void registerUser(Source source) {
//        String senderId = source.getSenderId();
//        UserProfileResponse sender = getProfile(senderId);
//        userService.registerUser(sender.getUserId(), sender.getDisplayName());
//    }

    /**
     * Reply menu flex.
     */
    public void replyFlexMenu(String replyToken) {
        String senderId = source.getSenderId();
        UserProfileResponse sender = getProfile(senderId);

//        FlexMessage flexMessage = botTemplate.createFlexMenu();
//        List<Message> messageList = new ArrayList<>();
//        messageList.add(new TextMessage("Hi "
//                + sender.getDisplayName() + ", apa yang ingin kamu lakukan ?"));
//        messageList.add(flexMessage);
//        reply(replyToken, messageList);
        replyText(replyToken, "Selamat datang!");
    }
//
//    /**
//     * Reply expense category flex.
//     */
//    public void relpyFlexChooseCategory(String replyToken) {
//        FlexMessage flexMessage = botTemplate.createFlexChooseCategory();
//        reply(replyToken, flexMessage);
//    }
//
//    public void replyFlexUbah(String replyToken) {
//        FlexMessage flexMessage = botTemplate.createFlexUbah();
//        reply(replyToken, flexMessage);
//    }
//
//    public void replyFlexLihatLaporan(String replyToken) {
//        FlexMessage flexMessage = botTemplate.createFlexLihatLaporan();
//        reply(replyToken, flexMessage);
//    }

    private void replyText(String replyToken, String message) {
        TextMessage textMessage = new TextMessage(message);
        reply(replyToken, textMessage);
    }

    public void reply(String replyToken, Message message) {
        ReplyMessage replyMessage = new ReplyMessage(replyToken, message);
        reply(replyMessage);
    }

//    public void reply(String replyToken, List<Message> message) {
//        ReplyMessage replyMessage = new ReplyMessage(replyToken, message);
//        reply(replyMessage);
//    }

    private void reply(ReplyMessage replyMessage) {
        try {
            lineMessagingClient.replyMessage(replyMessage).get();
        } catch (InterruptedException | ExecutionException | RuntimeException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get user data.
     */
    public UserProfileResponse getProfile(String userId) {
        try {
            return lineMessagingClient.getProfile(userId).get();
        } catch (InterruptedException | ExecutionException | RuntimeException e) {
            e.printStackTrace();
            return null;
        }
    }

//    /**
//     * Push message.
//     */
//    public void push(PushMessage pushMessage) {
//        try {
//            lineMessagingClient.pushMessage(pushMessage).get();
//        } catch (InterruptedException | ExecutionException | RuntimeException e) {
//            e.printStackTrace();
//        }
//    }

//    /**
//     * Send message to multiple user.
//     */
//    public void multicast(Set<String> to, Message message) {
//        try {
//            Multicast multicast = new Multicast(to, message);
//            lineMessagingClient.multicast(multicast).get();
//        } catch (InterruptedException | ExecutionException | RuntimeException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * Handle user request.
     */
    public void handleMessageEvent(MessageEvent messageEvent) {
        TextMessageContent textMessageContent = (TextMessageContent) messageEvent.getMessage();
        String replyToken = messageEvent.getReplyToken();
        String userMessage = textMessageContent.getText();
//        String senderId = source.getSenderId();

//        Handler oldHandler = currentHandler.get(senderId);
        if (userMessage.toLowerCase().equals("menu")) {
            replyText(replyToken, "Pesan masuk!");
        }
    }
}
