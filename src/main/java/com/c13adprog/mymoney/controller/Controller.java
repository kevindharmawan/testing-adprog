package com.c13adprog.mymoney.controller;

import com.c13adprog.mymoney.model.Events;
import com.c13adprog.mymoney.service.BotService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.client.LineSignatureValidator;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.ReplyEvent;
import com.linecorp.bot.model.objectmapper.ModelObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    @Autowired
    private BotService botService;

    @Autowired
    @Qualifier("lineMessagingClient")
    private LineMessagingClient lineMessagingClient;

    @Autowired
    @Qualifier("lineSignatureValidator")
    private LineSignatureValidator lineSignatureValidator;

    @RequestMapping(value="/webhook", method= RequestMethod.POST)
    public ResponseEntity<String> callback(
            @RequestHeader("X-Line-Signature") String xLineSignature,
            @RequestBody String eventsPayload) {
        try {
            if (!lineSignatureValidator.validateSignature(eventsPayload.getBytes(), xLineSignature)) {
                throw new RuntimeException("Invalid Signature Validation");
            }
            ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
            Events events = objectMapper.readValue(eventsPayload, Events.class);

            events.getEvents().forEach((event) -> {
                if (event instanceof FollowEvent) {
                    String replyToken = ((ReplyEvent) event).getReplyToken();
                    botService.greetingMessage(replyToken);
                }
                else if (event instanceof MessageEvent) {
                    botService.handleMessageEvent((MessageEvent) event);
                }
            });
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

//    @RequestMapping(value="/webhook", method= RequestMethod.POST)
//    public ResponseEntity<String> callback(
//            @RequestHeader("X-Line-Signature") String lineSignature,
//            @RequestBody String eventsPayload) {
//        try {
//            if (!lineSignatureValidator.validateSignature(eventsPayload.getBytes(), lineSignature)) {
//                throw new RuntimeException("Invalid Signature Validation");
//            }
//            handleEvent(eventsPayload, lineSignature);
//            return new ResponseEntity<>(HttpStatus.OK);
//        }
//        catch (Exception e) {
//            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//        }
//    }

//    public void handleEvent(String eventsPayload, String lineSignature)
//        throws JsonProcessingException {
//        ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
//        Events events = objectMapper.readValue(eventsPayload, Events.class);
//        events.getEvents().forEach(event -> {
//            if (event instanceof FollowEvent) {
//                String replyToken = ((FollowEvent) event).getReplyToken();
//                botService.source = event.getSource();
//                botService.greetingMessage(replyToken);
//            }
//            else if (event instanceof MessageEvent) {
//                botService.source = event.getSource();
//                botService.handleMessageEvent((MessageEvent) event);
//            }
//        });
//    }
}
