package com.c13adprog.mymoney.model;

public class Record {
    private String userId;
    private String name;
    private String category;
    private String timestamp;
    private String nominal;

    /**
     * Record for user's money
     **/
    public Record(String userId, String name,
                  String category, String timestamp, String nominal) {
        this.userId = userId;
        this.name = name;
        this.category = category;
        this.timestamp = timestamp;
        this.nominal = nominal;
    }

    /**
     * Setter
     **/
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    /**
     * Getter
     **/
    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getNominal() {
        return nominal;
    }
}
