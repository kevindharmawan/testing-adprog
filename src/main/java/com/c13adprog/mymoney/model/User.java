package com.c13adprog.mymoney.model;

public class User {
    private String userId;
    private String name;
    private String remind;

    /**
     * Line Bot user
     **/
    public User(String userId, String name, String remind) {
        this.userId = userId;
        this.name = name;
        this.remind = remind;
    }

    /**
     * Setter
     **/
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setReminder(String remind) {
        this.remind = remind;
    }

    /**
     * Getter
     **/
    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getReminder() {
        return remind;
    }
}