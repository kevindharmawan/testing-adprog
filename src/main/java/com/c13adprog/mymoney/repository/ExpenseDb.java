package com.c13adprog.mymoney.repository;

import com.c13adprog.mymoney.database.ExpenseDao;
import com.c13adprog.mymoney.model.Expense;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class ExpenseDb {

    @Autowired
    private ExpenseDao expenseDao;

    public int saveExpense(String description) {
        String[] data = description.split(";");
        LocalDateTime timestamp = LocalDateTime.now().plusHours(7);
        return expenseDao.saveExpense(
                data[0],
                data[1],
                data[2],
                String.valueOf(timestamp),
                data[3]
        );
    }

    public List<Expense> getByUserId(String userId) {
        return expenseDao.getByUserId(userId);
    }
}
