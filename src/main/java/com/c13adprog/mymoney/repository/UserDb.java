package com.c13adprog.mymoney.repository;

import com.c13adprog.mymoney.database.UserDao;
import com.c13adprog.mymoney.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class UserDb {

    @Autowired
    private UserDao userDao;

    /**
     * Input new user to database
     **/
    public int registerUser(String userId, String name) {
        if (getUserById(userId) == null) {
            return userDao.registerUser(userId, name);
        }
        return -1;
    }

    public String getUserById(String userId) {
        List<User> users = userDao.getByUserId("%" + userId + "%");
        if (users.size() > 0) {
            return users.get(0).getUserId();
        }
        return null;
    }

    public List<User> getAllUsers() {
        return userDao.get();
    }

    public List<User> getStatusNotifikasiByUserId(String userId) {
        return userDao.getStatusNotifikasiByUserId(userId);
    }

    public Set<String> getAllUserNotifikasiAktif() {
        List<User> users = userDao.getAllUserNotifikasiAktif();
        List<String> userIdList = new ArrayList<>();
        for (User user : users) {
            userIdList.add(user.getUserId());
        }
        return new HashSet(userIdList);
    }

    public int setStatusNotifikasi(String status, String userId) {
        return userDao.setStatusNotifikasiByUserId(status, userId);
    }
}