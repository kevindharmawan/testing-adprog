package com.c13adprog.mymoney.repository;

import com.c13adprog.mymoney.database.RecordDao;
import com.c13adprog.mymoney.model.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class RecordDb {

    @Autowired
    private RecordDao recordDao;

    /**
     * Input user record to database
     **/
    public int saveRecord(String description) {
        String[] userData = description.split(";");
        LocalDateTime localDateTime = LocalDateTime.now();
        localDateTime = localDateTime.plusHours(7);
        return recordDao.saveRecord(userData[0], userData[1],
                userData[2], String.valueOf(localDateTime), userData[3]);
    }

    public List<Record> getByUserId(String userId) {
        return recordDao.getByUserId(userId);
    }
}