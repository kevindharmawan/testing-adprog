package com.c13adprog.mymoney.handler.catatPendapatan;

import com.c13adprog.mymoney.handler.Handler;
import com.c13adprog.mymoney.handler.ResponseTemplate;

public class PendapatanKonfirmasiHandler extends ResponseTemplate implements Handler {

    public PendapatanKonfirmasiHandler(PendapatanNominalHandler state) {
        this.state = state;
    }

    @Override
    public ResponseTemplate verificationMessage(String message) {
        if (message.equalsIgnoreCase("ya")) {
            return handle(message);
        } else {
            return cancelOperation(message);
        }
    }

    @Override
    public ResponseTemplate handle(String message) {
        messageToUser = "Pendapatan kamu berhasil dicatat";
        description = "";
        return null;
    }

    @Override
    public ResponseTemplate unknownMessage() {
        messageToUser = "Konfirmasi pencatatan dengan menjawab 'Ya' "
                + "atau ketik 'Batal' untuk membatalkan tindakan";
        return this;
    }


    @Override
    public String getDescription() {
        return state.getDescription() + ";" + description;
    }
}
