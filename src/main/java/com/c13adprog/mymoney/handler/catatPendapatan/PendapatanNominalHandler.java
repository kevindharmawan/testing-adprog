package com.c13adprog.mymoney.handler.catatPendapatan;

import com.c13adprog.mymoney.handler.Handler;
import com.c13adprog.mymoney.handler.ResponseTemplate;

public class PendapatanNominalHandler extends ResponseTemplate implements Handler {

    public PendapatanNominalHandler(PendapatanKategoriHandler state) {
        this.state = state;
    }

    @Override
    public ResponseTemplate verificationMessage(String message) {
        try {
            return handle(message);
        } catch (NumberFormatException e) {
            return cancelOperation(message);
        }
    }

    @Override
    public ResponseTemplate handle(String message) {
        Integer.parseInt(message);
        description = message;
        messageToUser = "Oke, uang yang digunakan sebesar Rp" + message
                + ". Konfirmasi pencatatan dengan menjawab 'Ya' "
                + "atau ketik 'Batal' untuk membatalkan tindakan";
        return new PendapatanKonfirmasiHandler(this);
    }

    @Override
    public ResponseTemplate unknownMessage() {
        messageToUser = "Nominal uang tidak sesuai. "
                + "Pastikan kamu hanya memasukkan angka, contoh: 50000. "
                + "Jika ingin membatalkan tindakan, ketik 'Batal'";
        return this;
    }

    @Override
    public String getDescription() {
        return state.getDescription() + ";" + description;
    }

    private boolean isNominal(String userMessage) {
        try {
            return Integer.parseInt(userMessage) > 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
