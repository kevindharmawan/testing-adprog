package com.c13adprog.mymoney.handler.catatPendapatan;

import com.c13adprog.mymoney.handler.Handler;
import com.c13adprog.mymoney.handler.ResponseTemplate;

import java.util.Arrays;
import java.util.List;

public class PendapatanKategoriHandler extends ResponseTemplate implements Handler {
    final List<String> categories = Arrays.asList("Gaji", "Usaha", "lainnya");

    public PendapatanKategoriHandler(String senderId, String name) {
        this.description = senderId + ";" + name;
    }

    @Override
    public ResponseTemplate verificationMessage(String message) {
        if (categories.contains(message)) {
            return handle(message);
        } else {
            return cancelOperation(message);
        }
    }

    @Override
    public ResponseTemplate handle(String message) {
        description += ";" + message;
        messageToUser = "Kategori berhasil dipilih. "
                + "Berapa uang yang kamu habiskan untuk " + message + " ?";
        return new PendapatanNominalHandler(this);
    }

    @Override
    public ResponseTemplate unknownMessage() {
        messageToUser = "Tidak terdapat kategori tersebut! " +
                "Silakan pilih kembali kategori yang tersedia " +
                "Jika ingin membatalkan tindakan, ketik 'Batal'";
        return this;
    }

    @Override
    public String getDescription() {
        return this.description;
    }
}
