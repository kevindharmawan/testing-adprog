package com.c13adprog.mymoney.handler.laporan;

import com.c13adprog.mymoney.handler.Handler;
import com.c13adprog.mymoney.handler.ResponseTemplate;

import java.util.Arrays;
import java.util.List;

public class LaporanWaktuHandler extends ResponseTemplate implements Handler {
    public static final List<String> kategoriWaktu = Arrays.asList("Harian", "Mingguan", "Bulanan");
    LaporanCatatanHandler catatan;


    public LaporanWaktuHandler(LaporanCatatanHandler catatan) {
        this.catatan = catatan;
    }

    @Override
    public ResponseTemplate verificationMessage(String message) {
        if (kategoriWaktu.contains(message)) {
            return handle(message);
        } else {
            return cancelOperation(message);
        }
    }

    //TODO: Implements service
    @Override
    public ResponseTemplate handle(String message) {
        description += ";" + message;
        messageToUser = message + " telah dipilih.";
        return null;
    }

    @Override
    public ResponseTemplate unknownMessage() {
        messageToUser = "Kategori yang dipilih tidak ada, tolong pilih salah satu dari kategori yang tersedia."
                + " Ketik 'Batal' jika ingin membatalkan.";
        return this;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

}
//TODO:get list data berdasarkan user, ambil data berdasarkan waktu dari string message
//                "Gaji    : . . ." +
//                "Usaha   : . . ." +
//                "Lainnya : . . .";
//

//TODO:get list data berdasarkan user, ambil data berdasarkan waktu dari string message
//                "Konsumsi       : . . ." +
//                "Transportasi   : . . ." +
//                "Utilitas       : . . ." +
//                "Belanja        : . . ." +
//                "Lainnya        : . . .";
//
//


