package com.c13adprog.mymoney.handler;

public interface Handler {
    ResponseTemplate verificationMessage(String message);

    ResponseTemplate handle(String message);

    String getDescription();
}
