package com.c13adprog.mymoney.handler;

public abstract class ResponseTemplate {
    protected Handler state;
    protected Handler nextHandler;
    protected String description;
    protected String messageToUser;

    public void setNextHandler(Handler handler) {
        this.nextHandler = handler;
    }

    public String getMessageToUser() {
        return messageToUser;
    }

    public ResponseTemplate cancelOperation(String message) {
        if (message.equalsIgnoreCase("batal")) {
            messageToUser = "Proses fitur dibatalkan";
            return null;
        } else {
            return unknownMessage();
        }

    }

    public abstract ResponseTemplate unknownMessage();
}
